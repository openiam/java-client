
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "issuer",
    "clientId",
    "authorization_endpoint",
    "token_endpoint",
    "token_endpoint_auth_methods_supported",
    "token_endpoint_auth_signing_alg_values_supported",
    "userinfo_endpoint",
    "jwks_uri",
    "scopes_supported",
    "response_types_supported",
    "subject_types_supported",
    "id_token_signing_alg_values_supported",
    "claims_parameter_supported"
})
@Generated("jsonschema2pojo")
public class OpenIAMDiscoveryData implements Serializable
{

    @JsonProperty("issuer")
    private String issuer;
    @JsonProperty("clientId")
    private String clientId;
    @JsonProperty("authorization_endpoint")
    private String authorizationEndpoint;
    @JsonProperty("token_endpoint")
    private String tokenEndpoint;
    @JsonProperty("token_endpoint_auth_methods_supported")
    private List<String> tokenEndpointAuthMethodsSupported = new ArrayList<String>();
    @JsonProperty("token_endpoint_auth_signing_alg_values_supported")
    private List<String> tokenEndpointAuthSigningAlgValuesSupported = new ArrayList<String>();
    @JsonProperty("userinfo_endpoint")
    private String userinfoEndpoint;
    @JsonProperty("jwks_uri")
    private String jwksUri;
    @JsonProperty("scopes_supported")
    private List<String> scopesSupported = new ArrayList<String>();
    @JsonProperty("response_types_supported")
    private List<String> responseTypesSupported = new ArrayList<String>();
    @JsonProperty("subject_types_supported")
    private List<String> subjectTypesSupported = new ArrayList<String>();
    @JsonProperty("id_token_signing_alg_values_supported")
    private List<String> idTokenSigningAlgValuesSupported = new ArrayList<String>();
    @JsonProperty("claims_parameter_supported")
    private Boolean claimsParameterSupported;
    private final static long serialVersionUID = 712144636163210342L;

    @JsonProperty("issuer")
    public String getIssuer() {
        return issuer;
    }

    @JsonProperty("issuer")
    public void setIssuer(String issuer) {
        this.issuer = issuer;
    }

    @JsonProperty("clientId")
    public String getClientId() {
        return clientId;
    }

    @JsonProperty("clientId")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @JsonProperty("authorization_endpoint")
    public String getAuthorizationEndpoint() {
        return authorizationEndpoint;
    }

    @JsonProperty("authorization_endpoint")
    public void setAuthorizationEndpoint(String authorizationEndpoint) {
        this.authorizationEndpoint = authorizationEndpoint;
    }

    @JsonProperty("token_endpoint")
    public String getTokenEndpoint() {
        return tokenEndpoint;
    }

    @JsonProperty("token_endpoint")
    public void setTokenEndpoint(String tokenEndpoint) {
        this.tokenEndpoint = tokenEndpoint;
    }

    @JsonProperty("token_endpoint_auth_methods_supported")
    public List<String> getTokenEndpointAuthMethodsSupported() {
        return tokenEndpointAuthMethodsSupported;
    }

    @JsonProperty("token_endpoint_auth_methods_supported")
    public void setTokenEndpointAuthMethodsSupported(List<String> tokenEndpointAuthMethodsSupported) {
        this.tokenEndpointAuthMethodsSupported = tokenEndpointAuthMethodsSupported;
    }

    @JsonProperty("token_endpoint_auth_signing_alg_values_supported")
    public List<String> getTokenEndpointAuthSigningAlgValuesSupported() {
        return tokenEndpointAuthSigningAlgValuesSupported;
    }

    @JsonProperty("token_endpoint_auth_signing_alg_values_supported")
    public void setTokenEndpointAuthSigningAlgValuesSupported(List<String> tokenEndpointAuthSigningAlgValuesSupported) {
        this.tokenEndpointAuthSigningAlgValuesSupported = tokenEndpointAuthSigningAlgValuesSupported;
    }

    @JsonProperty("userinfo_endpoint")
    public String getUserinfoEndpoint() {
        return userinfoEndpoint;
    }

    @JsonProperty("userinfo_endpoint")
    public void setUserinfoEndpoint(String userinfoEndpoint) {
        this.userinfoEndpoint = userinfoEndpoint;
    }

    @JsonProperty("jwks_uri")
    public String getJwksUri() {
        return jwksUri;
    }

    @JsonProperty("jwks_uri")
    public void setJwksUri(String jwksUri) {
        this.jwksUri = jwksUri;
    }

    @JsonProperty("scopes_supported")
    public List<String> getScopesSupported() {
        return scopesSupported;
    }

    @JsonProperty("scopes_supported")
    public void setScopesSupported(List<String> scopesSupported) {
        this.scopesSupported = scopesSupported;
    }

    @JsonProperty("response_types_supported")
    public List<String> getResponseTypesSupported() {
        return responseTypesSupported;
    }

    @JsonProperty("response_types_supported")
    public void setResponseTypesSupported(List<String> responseTypesSupported) {
        this.responseTypesSupported = responseTypesSupported;
    }

    @JsonProperty("subject_types_supported")
    public List<String> getSubjectTypesSupported() {
        return subjectTypesSupported;
    }

    @JsonProperty("subject_types_supported")
    public void setSubjectTypesSupported(List<String> subjectTypesSupported) {
        this.subjectTypesSupported = subjectTypesSupported;
    }

    @JsonProperty("id_token_signing_alg_values_supported")
    public List<String> getIdTokenSigningAlgValuesSupported() {
        return idTokenSigningAlgValuesSupported;
    }

    @JsonProperty("id_token_signing_alg_values_supported")
    public void setIdTokenSigningAlgValuesSupported(List<String> idTokenSigningAlgValuesSupported) {
        this.idTokenSigningAlgValuesSupported = idTokenSigningAlgValuesSupported;
    }

    @JsonProperty("claims_parameter_supported")
    public Boolean getClaimsParameterSupported() {
        return claimsParameterSupported;
    }

    @JsonProperty("claims_parameter_supported")
    public void setClaimsParameterSupported(Boolean claimsParameterSupported) {
        this.claimsParameterSupported = claimsParameterSupported;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(OpenIAMDiscoveryData.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("issuer");
        sb.append('=');
        sb.append(((this.issuer == null)?"<null>":this.issuer));
        sb.append(',');
        sb.append("clientId");
        sb.append('=');
        sb.append(((this.clientId == null)?"<null>":this.clientId));
        sb.append(',');
        sb.append("authorizationEndpoint");
        sb.append('=');
        sb.append(((this.authorizationEndpoint == null)?"<null>":this.authorizationEndpoint));
        sb.append(',');
        sb.append("tokenEndpoint");
        sb.append('=');
        sb.append(((this.tokenEndpoint == null)?"<null>":this.tokenEndpoint));
        sb.append(',');
        sb.append("tokenEndpointAuthMethodsSupported");
        sb.append('=');
        sb.append(((this.tokenEndpointAuthMethodsSupported == null)?"<null>":this.tokenEndpointAuthMethodsSupported));
        sb.append(',');
        sb.append("tokenEndpointAuthSigningAlgValuesSupported");
        sb.append('=');
        sb.append(((this.tokenEndpointAuthSigningAlgValuesSupported == null)?"<null>":this.tokenEndpointAuthSigningAlgValuesSupported));
        sb.append(',');
        sb.append("userinfoEndpoint");
        sb.append('=');
        sb.append(((this.userinfoEndpoint == null)?"<null>":this.userinfoEndpoint));
        sb.append(',');
        sb.append("jwksUri");
        sb.append('=');
        sb.append(((this.jwksUri == null)?"<null>":this.jwksUri));
        sb.append(',');
        sb.append("scopesSupported");
        sb.append('=');
        sb.append(((this.scopesSupported == null)?"<null>":this.scopesSupported));
        sb.append(',');
        sb.append("responseTypesSupported");
        sb.append('=');
        sb.append(((this.responseTypesSupported == null)?"<null>":this.responseTypesSupported));
        sb.append(',');
        sb.append("subjectTypesSupported");
        sb.append('=');
        sb.append(((this.subjectTypesSupported == null)?"<null>":this.subjectTypesSupported));
        sb.append(',');
        sb.append("idTokenSigningAlgValuesSupported");
        sb.append('=');
        sb.append(((this.idTokenSigningAlgValuesSupported == null)?"<null>":this.idTokenSigningAlgValuesSupported));
        sb.append(',');
        sb.append("claimsParameterSupported");
        sb.append('=');
        sb.append(((this.claimsParameterSupported == null)?"<null>":this.claimsParameterSupported));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
