package org.openiam.apiclient.model.exception;

public class OpenIAMLoginException extends Exception{


    public OpenIAMLoginException(String message) {
        super(message);
    }
}
