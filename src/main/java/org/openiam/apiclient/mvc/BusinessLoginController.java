package org.openiam.apiclient.mvc;

import org.openiam.apiclient.model.openiam.Error;
import org.openiam.apiclient.model.openiam.OpenIAMBaseResponse;
import org.openiam.apiclient.model.openiam.OpenIAMBeanResponse;
import org.openiam.apiclient.model.openiam.OpenIAMCreateUserRequest;
import org.openiam.apiclient.service.OpenIAMService;
import org.openiam.apiclient.utils.CollectionUtils;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@Controller
public class BusinessLoginController {

    private final OpenIAMService openIAMService;

    public BusinessLoginController(OpenIAMService openIAMService) {
        this.openIAMService = openIAMService;
    }

    /**
     * Render creare new user page. Prepopulate user types,  roles and groups to the page with OpenIAM Call.
     *
     * @param request - current Http Request {@link HttpServletRequest}
     * @return - view name to render
     */
    @GetMapping("/user")
    public String getUserPage(HttpServletRequest request) {
        OpenIAMBeanResponse userTypes = openIAMService.getMetadataTypes("USER_OBJECT_TYPE", 0, 100);
        OpenIAMBeanResponse emailTypes = openIAMService.getMetadataTypes("EMAIL", 0, 100);
        OpenIAMBeanResponse phoneTypes = openIAMService.getMetadataTypes("PHONE", 0, 100);
        OpenIAMBeanResponse roles = openIAMService.findRoles(null, 0, 100);
        OpenIAMBeanResponse groups = openIAMService.findGroups(null, 0, 100);

        request.setAttribute("METADATA_TYPES", userTypes.getBeans());
        request.setAttribute("GROUPS", groups.getBeans());
        request.setAttribute("ROLES", roles.getBeans());
        request.setAttribute("EMAIL_TYPES", emailTypes.getBeans());
        request.setAttribute("PHONE_TYPES", phoneTypes.getBeans());

        return "user";
    }

    @PostMapping(value = "/user", consumes = "application/json")
    public String getUserPage(
            HttpServletRequest request,
            @RequestBody OpenIAMCreateUserRequest data) throws IOException {
        OpenIAMBaseResponse openIAMBaseResponse = openIAMService.createUser(data);
        if (openIAMBaseResponse.getError() && !CollectionUtils.isEmpty(openIAMBaseResponse.getErrorList())) {
            request.setAttribute("errors", openIAMBaseResponse.getErrorList().stream().map(Error::getMessage).collect(Collectors.toList()));
            return "error";
        } else {
            request.setAttribute("success", openIAMBaseResponse.getSuccessMessage());
            request.setAttribute("data", openIAMBaseResponse.getPrimaryKey());
            return "success";
        }
    }

}
