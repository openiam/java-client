package org.openiam.apiclient.service.impl;

import org.openiam.apiclient.model.BaseClientAppResponse;
import org.openiam.apiclient.model.ClientAppSessionDetails;
import org.openiam.apiclient.model.OpenIAMAuthorizationCallbackResponse;
import org.openiam.apiclient.model.openiam.Error;
import org.openiam.apiclient.model.openiam.GetAccessTokenResponse;
import org.openiam.apiclient.model.openiam.OpenIAMBaseResponse;
import org.openiam.apiclient.model.exception.OpenIAMLoginException;
import org.openiam.apiclient.service.OpenIAMService;
import org.openiam.apiclient.service.SessionManagementService;
import org.openiam.apiclient.utils.StringUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

@Service
public class SessionManagementServiceImpl implements SessionManagementService {

    /**
     * This is current InMemoryStorage where a Key is current client App SessionId and a Value is OpenIAM Access_token.
     */
    private final static Map<String, ClientAppSessionDetails> TOKEN_INMEMORY_STORAGE = new ConcurrentHashMap<>();

    private final OpenIAMService openIAMService;

    public SessionManagementServiceImpl(OpenIAMService openIAMService) {
        this.openIAMService = openIAMService;
    }

    public String getSessionId(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        Cookie cookie = cookies == null
                ? null
                : Arrays.stream(cookies)
                .filter(it -> it.getName().equals(COOKIE_NAME)).findFirst().orElse(null);
        return cookie == null ? null : cookie.getValue();
    }

    @Override
    public boolean isAuthenticated(HttpServletRequest request) {
        final String sessionId = getSessionId(request);
        return sessionId != null && TOKEN_INMEMORY_STORAGE.get(sessionId) != null;
    }

    @Override
    public void saveSessionCookie(HttpServletResponse response, String sessionId) {
        response.addCookie(new Cookie(COOKIE_NAME, sessionId));
    }


    private ClientAppSessionDetails getSessionDetails(OpenIAMBaseResponse openIAMLoginResponse) {
        final ClientAppSessionDetails sessionDetails = new ClientAppSessionDetails();
        sessionDetails.setUserId(openIAMLoginResponse.getUserId());
        if (openIAMLoginResponse.getTokenInfo() != null && openIAMLoginResponse.getTokenInfo().getAuthToken() != null) {
            sessionDetails.setOpeniamCookieToken(openIAMLoginResponse.getTokenInfo().getAuthToken());
        }
        return sessionDetails;
    }

    private void setOAuthDetailsToTheSession(ClientAppSessionDetails sessionDetails, GetAccessTokenResponse tokenResponse) {
        sessionDetails.setOpeniamOAuthToken(tokenResponse.getAccessToken());
        // minus one minute to life use refresh token before expiration of the current access token
        sessionDetails.setExpireAt(System.currentTimeMillis() + (tokenResponse.getExpiresIn() - 60) * 1000L);
        sessionDetails.setOpeniamOAuthRefreshToken(tokenResponse.getRefreshToken());
    }


    @Override
    public BaseClientAppResponse authenticate(HttpServletRequest request) {
        final OpenIAMBaseResponse loginResponse;
        final BaseClientAppResponse clientAppResponse = new BaseClientAppResponse();
        //Do Login
        try {
            loginResponse = openIAMService.doLogin(request.getParameter("login"), request.getParameter("password"));
        } catch (OpenIAMLoginException e) {
            clientAppResponse.setErrors(Collections.singletonList(e.getMessage()));
            return clientAppResponse.failed();
        }
        //if Login Throws error, then stop authentication and show error on the screen
        if (loginResponse.getError()) {
            if (loginResponse.getErrorList() != null) {
                clientAppResponse.setErrors(loginResponse.getErrorList().stream().map(Error::getMessage).collect(Collectors.toList()));
            } else {
                clientAppResponse.setErrors(Collections.singletonList("Server error"));
            }
            return clientAppResponse.failed();
        }
        //we fill the response with Success Login Information
        ClientAppSessionDetails sessionDetails = getSessionDetails(loginResponse);
        //We have auth Token, Keep this in the storage
        final String newSessionId = addSessionDetails(sessionDetails);

        // Here we are trying to get oauth2 access token to be able to use REST API
        final String cookieValue = sessionDetails.getOpeniamCookieToken();
        final OpenIAMAuthorizationCallbackResponse authorizationCallbackResponse = openIAMService
                .doAuthorization(
                        cookieValue,
                        newSessionId);
        if (authorizationCallbackResponse == null || !authorizationCallbackResponse.valid(newSessionId)) {
            clientAppResponse.setErrors(Collections.singletonList("Unable to authorize"));
            return clientAppResponse.failed();
        }
        //get access token with the 'code' got on authorization step
        GetAccessTokenResponse tokenResponse = openIAMService.doGetAccessToken(authorizationCallbackResponse.getCode(),
                cookieValue);

        setOAuthDetailsToTheSession(sessionDetails, tokenResponse);
        updateSessionDetails(newSessionId, sessionDetails);
        clientAppResponse.setSessionId(newSessionId);
        return clientAppResponse.succeed();
    }

    /**
     * Get current Session Details
     *
     * @param request - current Http Request
     * @return {@link ClientAppSessionDetails} for the provided session Id, null, otherwise.
     */
    @Override
    public ClientAppSessionDetails getCurrentSessionDetails(HttpServletRequest request) {
        return TOKEN_INMEMORY_STORAGE.get(getSessionId(request));

    }

    @Override
    public ClientAppSessionDetails refreshOAuthAccessToken(HttpServletRequest request) {
        ClientAppSessionDetails details = getCurrentSessionDetails(request);
        GetAccessTokenResponse response = openIAMService.doRefreshToken(details.getOpeniamOAuthRefreshToken());
        if (response != null) {
            setOAuthDetailsToTheSession(details, response);
            updateSessionDetails(getSessionId(request), details);
        }
        return details;
    }

    @Override
    public Boolean logout(HttpServletRequest request) {
        ClientAppSessionDetails details = getCurrentSessionDetails(request);
        Boolean revokeAccessToken = openIAMService.revokeToken(
                details.getOpeniamOAuthToken(),
                details.getOpeniamCookieToken());
        if (Boolean.TRUE.equals(revokeAccessToken)) {
            TOKEN_INMEMORY_STORAGE.remove(getSessionId(request));
            return true;
        }
        return false;
    }

    /**
     * Add new Details for new Session.
     *
     * @param data - data to be added to the session storage {@link ClientAppSessionDetails}
     * @return - the sessionId of newly saved session details
     */
    public String addSessionDetails(ClientAppSessionDetails data) {
        final String newSessionId = StringUtils.randomString(32);
        TOKEN_INMEMORY_STORAGE.put(newSessionId, data);
        return newSessionId;
    }

    /**
     * Add new Details for new Session.
     *
     * @param data - data to be added to the session storage {@link ClientAppSessionDetails}
     * @return - the sessionId of newly saved session details
     */
    public String updateSessionDetails(String sessionId, ClientAppSessionDetails data) {
        TOKEN_INMEMORY_STORAGE.put(sessionId, data);
        return sessionId;
    }


}
