package org.openiam.apiclient.model;

import java.io.Serializable;
import java.util.List;

public class BaseClientAppResponse implements Serializable {
    private static final long serialVersionUID = -7972041836498186628L;
    private boolean success;
    private String sessionId;
    private List<String> errors;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public BaseClientAppResponse succeed() {
        this.success = true;
        return this;
    }


    public BaseClientAppResponse failed() {
        this.success = false;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<String> getErrors() {
        return errors;
    }

    public void setErrors(List<String> errors) {
        this.errors = errors;
    }


}
