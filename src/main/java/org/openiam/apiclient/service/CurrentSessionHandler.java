package org.openiam.apiclient.service;

import org.openiam.apiclient.model.ClientAppSessionDetails;

/**
 * is Used to keep current user details in the current Java Thread to be able to use OpenIAM credentials in different parts of the application
 */
public class CurrentSessionHandler {
    private final ThreadLocal<ClientAppSessionDetails> detailsThreadLocal = new ThreadLocal<>();

    /**
     * Get Current {@link ClientAppSessionDetails} for current user
     *
     * @return {@link ClientAppSessionDetails}
     */
    public ClientAppSessionDetails getDetails() {
        return detailsThreadLocal.get();
    }

    /**
     * Assin {@link ClientAppSessionDetails} to the {@link Thread}
     *
     * @param details that should be set
     */
    public void setDetailsThreadLocal(ClientAppSessionDetails details) {
        detailsThreadLocal.set(details);
    }
}
