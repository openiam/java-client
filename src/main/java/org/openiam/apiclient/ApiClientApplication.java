package org.openiam.apiclient;

import org.openiam.apiclient.model.openiam.OpenIAMDiscoveryData;
import org.openiam.apiclient.service.CurrentSessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.web.reactive.function.client.WebClient;

@SpringBootApplication
public class ApiClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApiClientApplication.class, args);
    }

    @Value("${openiamOAuthDiscoveryUrl}")
    private String discoveryUrl;


    @Bean
    public OpenIAMDiscoveryData openIAMDiscoveryData() {
        ResponseEntity<OpenIAMDiscoveryData> entity = WebClient
                .create()
                .get()
                .uri(discoveryUrl)
                .retrieve()
                .toEntity(OpenIAMDiscoveryData.class)
                .block();
        if (entity == null || entity.getStatusCodeValue() != 200) {
            throw new IllegalStateException(String.format("Can't OpenDiscovery URL: %s", discoveryUrl));
        }
        return entity.getBody();
    }

    @Bean
    public CurrentSessionHandler sessionHandler(){
        return new CurrentSessionHandler();
    }
}
