
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "areaCd",
    "phoneNbr",
    "phoneExt",
    "typeId",
    "countryCd",
    "usedForSMS",
    "published"
})
@Generated("jsonschema2pojo")
public class Phone implements Serializable
{

    @JsonProperty("areaCd")
    private String areaCd;
    @JsonProperty("phoneNbr")
    private String phoneNbr;
    @JsonProperty("phoneExt")
    private String phoneExt;
    @JsonProperty("typeId")
    private String typeId;
    @JsonProperty("countryCd")
    private String countryCd;
    @JsonProperty("usedForSMS")
    private Boolean usedForSMS;
    @JsonProperty("published")
    private Boolean published;
    private final static long serialVersionUID = -2326205887009388254L;

    @JsonProperty("areaCd")
    public String getAreaCd() {
        return areaCd;
    }

    @JsonProperty("areaCd")
    public void setAreaCd(String areaCd) {
        this.areaCd = areaCd;
    }

    @JsonProperty("phoneNbr")
    public String getPhoneNbr() {
        return phoneNbr;
    }

    @JsonProperty("phoneNbr")
    public void setPhoneNbr(String phoneNbr) {
        this.phoneNbr = phoneNbr;
    }

    @JsonProperty("phoneExt")
    public String getPhoneExt() {
        return phoneExt;
    }

    @JsonProperty("phoneExt")
    public void setPhoneExt(String phoneExt) {
        this.phoneExt = phoneExt;
    }

    @JsonProperty("typeId")
    public String getTypeId() {
        return typeId;
    }

    @JsonProperty("typeId")
    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    @JsonProperty("countryCd")
    public String getCountryCd() {
        return countryCd;
    }

    @JsonProperty("countryCd")
    public void setCountryCd(String countryCd) {
        this.countryCd = countryCd;
    }

    @JsonProperty("usedForSMS")
    public Boolean getUsedForSMS() {
        return usedForSMS;
    }

    @JsonProperty("usedForSMS")
    public void setUsedForSMS(Boolean usedForSMS) {
        this.usedForSMS = usedForSMS;
    }

    @JsonProperty("published")
    public Boolean getPublished() {
        return published;
    }

    @JsonProperty("published")
    public void setPublished(Boolean published) {
        this.published = published;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(Phone.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("areaCd");
        sb.append('=');
        sb.append(((this.areaCd == null)?"<null>":this.areaCd));
        sb.append(',');
        sb.append("phoneNbr");
        sb.append('=');
        sb.append(((this.phoneNbr == null)?"<null>":this.phoneNbr));
        sb.append(',');
        sb.append("phoneExt");
        sb.append('=');
        sb.append(((this.phoneExt == null)?"<null>":this.phoneExt));
        sb.append(',');
        sb.append("typeId");
        sb.append('=');
        sb.append(((this.typeId == null)?"<null>":this.typeId));
        sb.append(',');
        sb.append("countryCd");
        sb.append('=');
        sb.append(((this.countryCd == null)?"<null>":this.countryCd));
        sb.append(',');
        sb.append("usedForSMS");
        sb.append('=');
        sb.append(((this.usedForSMS == null)?"<null>":this.usedForSMS));
        sb.append(',');
        sb.append("published");
        sb.append('=');
        sb.append(((this.published == null)?"<null>":this.published));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
