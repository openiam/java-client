package org.openiam.apiclient.filter;

import org.openiam.apiclient.model.ClientAppSessionDetails;
import org.openiam.apiclient.service.CurrentSessionHandler;
import org.openiam.apiclient.service.SessionManagementService;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.function.Supplier;
import java.util.stream.Stream;

/**
 * Filter is used to detect, does current client authorized against OpenIAM, if not, then force redirect to the Login.
 */
@Component
@Order(1)
public class OpenIAMAuthorizationTokenFilter implements Filter {

    private final SessionManagementService sessionManagementService;
    private final CurrentSessionHandler sessionHandler;

    //The Url of the Login page. Here we will redirect in case the Cookie doesn't exists or expires.
    public final static String LOGIN_URL = "/login";
    //The name of  Param is used for redirection
    public final static String REDIRECT_PARAM = "redirectTo";


    //The static list of Public URLs, to which we don't need authn/authz access.
    private final static String[] PUBLIC_URLS = {LOGIN_URL, "/logout", "/oauth/callback"};
    private final static Supplier<Stream<String>> stream = () -> Arrays.stream(PUBLIC_URLS);


    public OpenIAMAuthorizationTokenFilter(SessionManagementService sessionManagementService, CurrentSessionHandler sessionHandler) {
        this.sessionManagementService = sessionManagementService;
        this.sessionHandler = sessionHandler;
    }


    /**
     * Method is used to detect does current URL is public or Not.
     *
     * @param request - the Current Http Request.
     * @return - true if url is public, otherwise - false.
     */
    private boolean isPublicUrl(HttpServletRequest request) {
        return stream.get().anyMatch(it -> request.getRequestURI().equalsIgnoreCase(it));
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        final HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;

        if (isPublicUrl(httpServletRequest)) {
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if (!sessionManagementService.isAuthenticated(httpServletRequest)) {
            httpServletResponse.sendRedirect("/login?".concat(REDIRECT_PARAM).concat("=").concat(URLEncoder.encode(httpServletRequest.getRequestURI(), Charset.defaultCharset())));
            return;
        }
        ClientAppSessionDetails sessionDetails = sessionManagementService.getCurrentSessionDetails(httpServletRequest);
        //If access token is expired, than get new tokens with Refresh token
        if (sessionDetails.getExpireAt() < System.currentTimeMillis()) {
            sessionManagementService.refreshOAuthAccessToken(httpServletRequest);
            //this is redundantly for In Memory storage, but for common implementation we should re-read data that was updated.
            sessionDetails = sessionManagementService.getCurrentSessionDetails(httpServletRequest);
        }
        sessionHandler.setDetailsThreadLocal(sessionDetails);
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
