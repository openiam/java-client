package org.openiam.apiclient.model;

import org.openiam.apiclient.utils.StringUtils;

import java.io.Serializable;

public class OpenIAMAuthorizationCallbackResponse implements Serializable {
    private static final long serialVersionUID = 1607283496134760384L;
    private String code;
    private String sessionId;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean valid(String sessionId) {
        if (sessionId == null) {
            return code != null;
        } else {
            return code != null && sessionId.equals(this.sessionId);
        }
    }
}
