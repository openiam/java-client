# README #

# OpenIAM OAuth Java Client

This application is build to demonstrate HOWTO use OpenIAM REST API since v 4.2.1. It contains two parts:

- Authentication and access token management
- OpenIAM REST API calls.

We use Java implementation [OpenJDK11](https://openjdk.java.net/projects/jdk/11/)
The Application use [Maven 3.8](https://maven.apache.org/download.cgi#downloading-apache-maven-3-8-1) as a package
manager
[Spring Boot](https://spring.io/projects/spring-boot) is used to create an application

For Server-to-server communication [spring-boot-starter-webflux](https://spring.io/guides/gs/reactive-rest-service/) is
used.

## Pre-installation

### OpenIAM configuration

To use this client application please create new OAuth Client Authentication Provider in you OpenIAM instance. There is
important that the following configuration is set as below:

- Authorization Grant Flow is **Authorization Code**
- Client Authentication Type is **Request Body**
- Use Refresh Token is **Use Refresh token**
- Default Scopes contain is **/idp/oauth2/userinfo, /idp/oauth2/revoke, /webconsole/rest/api/***

Other options can be set up based on your preferences.

### Client Application (this) configuration

Please navigate to [application.properties](./src/main/resources/application.properties) file and configure the
properties based on your OpenIAM installation.

| Name | Description | Example Value | 
| ---- | ---- | ---- |
| openiamServerAddress | The base OpenIAM hostname with protocol and port | https://openiam.com, http://localhost:8080, http://test.domain.com | 
| openiamOAuthSecret | The **Client Secret** Value from OpenIAM Auth Provider | fb5c116b000bc166c55a1210d610cacd1b00005ceafc000bab5c02f2773e241 |
| openiamOAuthPostbackUrl | The **Redirect Url** Value from OpenIAM Auth Provider | http://openiam.com:8080/oauth/callback |
| openiamOAuthDiscoveryUrl | The **OpenID Connect Discovery URL (Readonly)** Value from OpenIAM Auth Provider | http://officedepotpoc.openiam.com/idp/oauth2/client-app/.well-known/openid-configuration |
| openiamAuthenticationCookieName | The **Authentication Cookie Name: ** Value from OpenIAM Content Provider | OPENIAM_AUTH_TOKEN (this is value for 99.99% use cases, please keep it the same) | 

The main Class of the application is located in the
class [org.openiam.apiclient.ApiClientApplication](./src/main/java/org/openiam/apiclient/ApiClientApplication.java)

to Compile and Run the application perform:

```shell
mvn clean install
java -jar target/api-client-0.0.1-SNAPSHOT.jar
```

To access the Application in the browser please hit [http://localhost:8080](http://localhost:8080)
and use credentials to OpenIAM install which you defined on Pre-installation step

Please be sure that Port 8080 is open

You can also generate Javadoc. Just run in the home project folder:

```shell
mvn javadoc:javadoc
mvn javadoc:aggregate
```

The javadocs will be available in folder target/site/apidocs/ the main file is index.html. You can open it with your
favorite browser.

## Authentication and access token management

The Main class, where communication with OpenIAM is define in
[org.openiam.apiclient.service.OpenIAMService](./src/main/java/org/openiam/apiclient/service/OpenIAMService.java)
interface and implemented in the class
[org.openiam.apiclient.service.impl.OpenIAMServiceImpl](./src/main/java/org/openiam/apiclient/service/impl/OpenIAMServiceImpl.java)

The class has following methods:

- Login

```java
 /**
 * Login against OpenIAM, send login and password. will return {@link OpenIAMLoginResponse} in case of error or success.
 *
 * @param login    - login
 * @param password - password
 * @return {@link OpenIAMLoginResponse} with errors or with OpenIAM cookie token in {@link OpenIAMLoginResponse#getTokenInfo()}
 * @throws OpenIAMLoginException
 */
    OpenIAMLoginResponse doLogin(String login,String password)throws OpenIAMLoginException;
```

- Authorize

```java
 /**
 * Authorize user against OpenIAM Oauth call.
 *
 * @param cookie    - the OpenIAM authentication cookie
 * @param sessionId - the current client app sessionId, identified with the cookie.
 * @return {@link OpenIAMAuthorizationCallbackResponse} with #sessionId and authorization code.
 */
 OpenIAMAuthorizationCallbackResponse doAuthorization(String cookie,String sessionId);
```  

- Get Access Token

```java
 /**
 * Get Access token from the OpenIAM.
 *
 * @param code   - the authirzation token retrieved with {@link OpenIAMService#doAuthorization(String, String)}
 * @param cookie - the OpenIAM authentication cookie
 * @return
 */
 GetAccessTokenResponse doGetAccessToken(String code,String cookie);
```

- Refresh access token using refresh Token

```java
 /**
 * Get New Access token based on refresh Token.
 *
 * @param refreshToken - the refresh token provided with Response by {@link OpenIAMService#doAuthorization(String, String)}
 * @return {@link GetAccessTokenResponse} with new Access token and Refresh Token
 */
 GetAccessTokenResponse doRefreshToken(String refreshToken);
```

- Revoke token

```java
 /**
 * Revoke Oauth2 access token against OpenIAM
 *
 * @param token  - the access token provided to be revoked.
 * @param cookie - OpenIAM Authentication Cookie
 * @return boolean in case revokation was success
 */
    boolean revokeToken(String token,String cookie);
```

- Create New User. In any cases there are 3 mandatory fields that MUST be set to any OpenIAM user. There are **firstName**, **lastName**, **metadataTypeId**
```java
 /**
 * Method is used to create a new User In OpenIAM
 *
 * @param openIAMCreateUserRequest - the {@link OpenIAMCreateUserRequest} with data for user to be saved.
 * @return {@link OpenIAMBaseResponse}. To detect the success please check {@link OpenIAMBaseResponse#getStatus()}.
 * Status 200 is a feature of success.
 * {@link OpenIAMBaseResponse#getErrorList()} contains an errors in case of {@link OpenIAMBaseResponse#getStatus()} is not equals 200
 */
    OpenIAMBaseResponse createUser(OpenIAMCreateUserRequest openIAMCreateUserRequest);
```  

- Find Roles by Name
```java
 /**
 * Use to search for Roles
 *
 * @param name - the term to search roles by name, can be null, so it will find all Roles (from,size)
 * @param from - very 1st request
 * @param size - max number of result per request
 * @return {@link OpenIAMBeanResponse} or null in case of any errors
 */
    OpenIAMBeanResponse findRoles(String name, int from, int size);
```

- Find Groups by Name
```java
 /**
 * Use to search for Groups
 *
 * @param name - the term to search groups by name, can be null, so it will find all Groups (from,size)
 * @param from - very 1st request
 * @param size - max number of result per request
 * @return {@link OpenIAMBeanResponse} or null in case of any errors
 */
    OpenIAMBeanResponse findGroups(String name, int from, int size);
```

- Find Metadata types by grouping. This is useful when you are creating users, due to **metadataTypeId** field is mandatory for new user.

```java
    /**
     * Use to search for Groups
     *
     * @param grouping - the grouping of MetadataType we should get
     * @param from - very 1st request
     * @param size - max number of result per request
     * @return {@link OpenIAMBeanResponse} or null in case of any errors
     */
    OpenIAMBeanResponse getMetadataTypes(String grouping, int from, int size);
```


For each authenticated against OpenIAM user in current custom application there is unique sessionId which is stored in
the Cookies. After user has been successfully authenticated the sessionId is generated and set to the Cookie. On the
server side We store OpenIAM credentials assosiated with the sessionId in the InMemory storage. The current application
session Management is defined with interface
[org.openiam.apiclient.service.SessionManagementService](./src/main/java/org/openiam/apiclient/service/SessionManagementService.java)
interface and implemented in the class
[org.openiam.apiclient.service.impl.SessionManagementServiceImpl](./src/main/java/org/openiam/apiclient/service/impl/SessionManagementServiceImpl.java)

 