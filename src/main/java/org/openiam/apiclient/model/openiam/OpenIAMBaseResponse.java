
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "primaryKey",
        "status",
        "errorList",
        "redirectURL",
        "successToken",
        "successMessage",
        "contextValues",
        "stackTrace",
        "possibleErrors",
        "passwordExpired",
        "userId",
        "unlockURL",
        "tokenInfo",
        "error"
})
@Generated("jsonschema2pojo")
public class OpenIAMBaseResponse implements Serializable {

    @JsonProperty("primaryKey")
    private Object primaryKey;
    @JsonProperty("status")
    private Integer status;
    @JsonProperty("errorList")
    private List<Error> errorList = new ArrayList<Error>();
    @JsonProperty("redirectURL")
    private String redirectURL;
    @JsonProperty("successToken")
    private Object successToken;
    @JsonProperty("successMessage")
    private Object successMessage;
    @JsonProperty("contextValues")
    private Object contextValues;
    @JsonProperty("stackTrace")
    private Object stackTrace;
    @JsonProperty("possibleErrors")
    private Object possibleErrors;
    @JsonProperty("passwordExpired")
    private Boolean passwordExpired;
    @JsonProperty("userId")
    private String userId;
    @JsonProperty("unlockURL")
    private Object unlockURL;
    @JsonProperty("tokenInfo")
    private TokenInfo tokenInfo;
    @JsonProperty("error")
    private Boolean error;
    private final static long serialVersionUID = -7498380229361235472L;

    @JsonProperty("primaryKey")
    public Object getPrimaryKey() {
        return primaryKey;
    }

    @JsonProperty("primaryKey")
    public void setPrimaryKey(Object primaryKey) {
        this.primaryKey = primaryKey;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("errorList")
    public List<Error> getErrorList() {
        return errorList;
    }

    @JsonProperty("errorList")
    public void setErrorList(List<Error> errorList) {
        this.errorList = errorList;
    }

    @JsonProperty("redirectURL")
    public String getRedirectURL() {
        return redirectURL;
    }

    @JsonProperty("redirectURL")
    public void setRedirectURL(String redirectURL) {
        this.redirectURL = redirectURL;
    }

    @JsonProperty("successToken")
    public Object getSuccessToken() {
        return successToken;
    }

    @JsonProperty("successToken")
    public void setSuccessToken(Object successToken) {
        this.successToken = successToken;
    }

    @JsonProperty("successMessage")
    public Object getSuccessMessage() {
        return successMessage;
    }

    @JsonProperty("successMessage")
    public void setSuccessMessage(Object successMessage) {
        this.successMessage = successMessage;
    }

    @JsonProperty("contextValues")
    public Object getContextValues() {
        return contextValues;
    }

    @JsonProperty("contextValues")
    public void setContextValues(Object contextValues) {
        this.contextValues = contextValues;
    }

    @JsonProperty("stackTrace")
    public Object getStackTrace() {
        return stackTrace;
    }

    @JsonProperty("stackTrace")
    public void setStackTrace(Object stackTrace) {
        this.stackTrace = stackTrace;
    }

    @JsonProperty("possibleErrors")
    public Object getPossibleErrors() {
        return possibleErrors;
    }

    @JsonProperty("possibleErrors")
    public void setPossibleErrors(Object possibleErrors) {
        this.possibleErrors = possibleErrors;
    }

    @JsonProperty("passwordExpired")
    public Boolean getPasswordExpired() {
        return passwordExpired;
    }

    @JsonProperty("passwordExpired")
    public void setPasswordExpired(Boolean passwordExpired) {
        this.passwordExpired = passwordExpired;
    }

    @JsonProperty("userId")
    public String getUserId() {
        return userId;
    }

    @JsonProperty("userId")
    public void setUserId(String userId) {
        this.userId = userId;
    }

    @JsonProperty("unlockURL")
    public Object getUnlockURL() {
        return unlockURL;
    }

    @JsonProperty("unlockURL")
    public void setUnlockURL(Object unlockURL) {
        this.unlockURL = unlockURL;
    }

    @JsonProperty("tokenInfo")
    public TokenInfo getTokenInfo() {
        return tokenInfo;
    }

    @JsonProperty("tokenInfo")
    public void setTokenInfo(TokenInfo tokenInfo) {
        this.tokenInfo = tokenInfo;
    }

    @JsonProperty("error")
    public boolean getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(boolean error) {
        this.error = error;
    }

    @Override
    public String toString() {
        return "OpenIAMLoginResponse{" +
                "primaryKey=" + primaryKey +
                ", status=" + status +
                ", errorList=" + errorList +
                ", redirectURL='" + redirectURL + '\'' +
                ", successToken=" + successToken +
                ", successMessage=" + successMessage +
                ", contextValues=" + contextValues +
                ", stackTrace=" + stackTrace +
                ", possibleErrors=" + possibleErrors +
                ", passwordExpired=" + passwordExpired +
                ", userId='" + userId + '\'' +
                ", unlockURL=" + unlockURL +
                ", tokenInfo=" + tokenInfo +
                ", error=" + error +
                '}';
    }
}
