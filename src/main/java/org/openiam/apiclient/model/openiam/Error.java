
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "i18nError",
    "error",
    "validationError",
    "params",
    "message"
})
@Generated("jsonschema2pojo")
public class Error implements Serializable
{

    @JsonProperty("i18nError")
    private Object i18nError;
    @JsonProperty("error")
    private String error;
    @JsonProperty("validationError")
    private Object validationError;
    @JsonProperty("params")
    private Object params;
    @JsonProperty("message")
    private String message;
    private final static long serialVersionUID = -215267217245829157L;

    @JsonProperty("i18nError")
    public Object getI18nError() {
        return i18nError;
    }

    @JsonProperty("i18nError")
    public void setI18nError(Object i18nError) {
        this.i18nError = i18nError;
    }

    @JsonProperty("error")
    public String getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(String error) {
        this.error = error;
    }

    @JsonProperty("validationError")
    public Object getValidationError() {
        return validationError;
    }

    @JsonProperty("validationError")
    public void setValidationError(Object validationError) {
        this.validationError = validationError;
    }

    @JsonProperty("params")
    public Object getParams() {
        return params;
    }

    @JsonProperty("params")
    public void setParams(Object params) {
        this.params = params;
    }

    @JsonProperty("message")
    public String getMessage() {
        return message;
    }

    @JsonProperty("message")
    public void setMessage(String message) {
        this.message = message;
    }

}
