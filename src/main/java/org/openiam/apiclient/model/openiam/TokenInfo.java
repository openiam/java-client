
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "authToken",
    "timeToLiveSeconds"
})
@Generated("jsonschema2pojo")
public class TokenInfo implements Serializable
{

    @JsonProperty("authToken")
    private String authToken;
    @JsonProperty("timeToLiveSeconds")
    private Integer timeToLiveSeconds;
    private final static long serialVersionUID = -7277527640559846679L;

    @JsonProperty("authToken")
    public String getAuthToken() {
        return authToken;
    }

    @JsonProperty("authToken")
    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @JsonProperty("timeToLiveSeconds")
    public Integer getTimeToLiveSeconds() {
        return timeToLiveSeconds;
    }

    @JsonProperty("timeToLiveSeconds")
    public void setTimeToLiveSeconds(Integer timeToLiveSeconds) {
        this.timeToLiveSeconds = timeToLiveSeconds;
    }

}
