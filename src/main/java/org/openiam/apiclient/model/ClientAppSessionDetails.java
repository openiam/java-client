package org.openiam.apiclient.model;

import java.io.Serializable;

public class ClientAppSessionDetails implements Serializable {
    private static final long serialVersionUID = 2084106560203625920L;
    private String openiamCookieToken;
    private String openiamOAuthToken;
    private String openiamOAuthRefreshToken;
    private long expireAt;
    private String userId;

    public String getOpeniamOAuthRefreshToken() {
        return openiamOAuthRefreshToken;
    }

    public void setOpeniamOAuthRefreshToken(String openiamOAuthRefreshToken) {
        this.openiamOAuthRefreshToken = openiamOAuthRefreshToken;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getOpeniamCookieToken() {
        return openiamCookieToken;
    }

    public void setOpeniamCookieToken(String openiamCookieToken) {
        this.openiamCookieToken = openiamCookieToken;
    }

    public String getOpeniamOAuthToken() {
        return openiamOAuthToken;
    }

    public void setOpeniamOAuthToken(String openiamOAuthToken) {
        this.openiamOAuthToken = openiamOAuthToken;
    }

    public long getExpireAt() {
        return expireAt;
    }

    public void setExpireAt(long expireAt) {
        this.expireAt = expireAt;
    }
}
