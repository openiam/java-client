
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "error",
    "page",
    "from",
    "size",
    "pageSize",
    "beans",
    "emptySearchBean"
})
@Generated("jsonschema2pojo")
public class OpenIAMBeanResponse implements Serializable
{

    @JsonProperty("error")
    private Object error;
    @JsonProperty("page")
    private Integer page;
    @JsonProperty("from")
    private Integer from;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("pageSize")
    private Integer pageSize;
    @JsonProperty("beans")
    private List<Bean> beans = new ArrayList<Bean>();
    @JsonProperty("emptySearchBean")
    private Boolean emptySearchBean;
    private final static long serialVersionUID = -5973806937569570424L;

    @JsonProperty("error")
    public Object getError() {
        return error;
    }

    @JsonProperty("error")
    public void setError(Object error) {
        this.error = error;
    }

    @JsonProperty("page")
    public Integer getPage() {
        return page;
    }

    @JsonProperty("page")
    public void setPage(Integer page) {
        this.page = page;
    }

    @JsonProperty("from")
    public Integer getFrom() {
        return from;
    }

    @JsonProperty("from")
    public void setFrom(Integer from) {
        this.from = from;
    }

    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }

    @JsonProperty("pageSize")
    public Integer getPageSize() {
        return pageSize;
    }

    @JsonProperty("pageSize")
    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    @JsonProperty("beans")
    public List<Bean> getBeans() {
        return beans;
    }

    @JsonProperty("beans")
    public void setBeans(List<Bean> beans) {
        this.beans = beans;
    }

    @JsonProperty("emptySearchBean")
    public Boolean getEmptySearchBean() {
        return emptySearchBean;
    }

    @JsonProperty("emptySearchBean")
    public void setEmptySearchBean(Boolean emptySearchBean) {
        this.emptySearchBean = emptySearchBean;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(OpenIAMBeanResponse.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("error");
        sb.append('=');
        sb.append(((this.error == null)?"<null>":this.error));
        sb.append(',');
        sb.append("page");
        sb.append('=');
        sb.append(((this.page == null)?"<null>":this.page));
        sb.append(',');
        sb.append("from");
        sb.append('=');
        sb.append(((this.from == null)?"<null>":this.from));
        sb.append(',');
        sb.append("size");
        sb.append('=');
        sb.append(((this.size == null)?"<null>":this.size));
        sb.append(',');
        sb.append("pageSize");
        sb.append('=');
        sb.append(((this.pageSize == null)?"<null>":this.pageSize));
        sb.append(',');
        sb.append("beans");
        sb.append('=');
        sb.append(((this.beans == null)?"<null>":this.beans));
        sb.append(',');
        sb.append("emptySearchBean");
        sb.append('=');
        sb.append(((this.emptySearchBean == null)?"<null>":this.emptySearchBean));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
