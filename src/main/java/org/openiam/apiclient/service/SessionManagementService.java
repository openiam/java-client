package org.openiam.apiclient.service;

import org.openiam.apiclient.model.BaseClientAppResponse;
import org.openiam.apiclient.model.ClientAppSessionDetails;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * The Service which is used to managed current User Session
 */
public interface SessionManagementService {
    /**
     * The name of the Custom App Cookie where we store our client App Session ID
     */
    String COOKIE_NAME = "SPRING_SESSION_ID";

    /**
     * Method is used get Value of the current Session Id (From the Cookies)
     *
     * @param request - current http Request
     * @return - The value of current SessionId, null if there is no such
     */
    String getSessionId(HttpServletRequest request);

    /**
     * Method is used to check does current user's request is authenticated request.
     *
     * @param request
     * @return
     */
    boolean isAuthenticated(HttpServletRequest request);

    /**
     * Method is used to check does current user's request is authenticated request.
     *
     * @param response - the {@link HttpServletResponse} to save the cookie with my session Id in the browser
     */
    void saveSessionCookie(HttpServletResponse response, String sessionId);

    /**
     * Method is used to retrieve all required authn/ authz tokens from OpenIAM. It will get login/password from the http request, tries
     * do authentication with OpenIAM, and if it was success, than try to get from OpenIAM Access_token to be able to
     * call OpenIAM API
     *
     * @param request - current http Request
     * @return - true if authn and authz was success, false - otherwise
     */
    BaseClientAppResponse authenticate(HttpServletRequest request);


    /**
     * Get Current session details from the storage
     *
     * @param - the current Http Request
     * @return
     */
    ClientAppSessionDetails getCurrentSessionDetails(HttpServletRequest request);

    /**
     * Refresh OpenIAM OAuth2 token
     *
     * @param - request - current http Request
     * @return {@link ClientAppSessionDetails} with updated OAuth tokens
     */
    ClientAppSessionDetails refreshOAuthAccessToken(HttpServletRequest request);

    /**
     * Logout from Client App. It will revoke OpenIAM OAuth tokens and cleanup OpenIAM cookie.
     *
     * @param - request - current http Request
     * @return {@link Boolean#TRUE} in case of success.
     */
    Boolean logout(HttpServletRequest request);
}
