package org.openiam.apiclient.service.impl;

import org.openiam.apiclient.model.OpenIAMAuthorizationCallbackResponse;
import org.openiam.apiclient.model.exception.OpenIAMLoginException;
import org.openiam.apiclient.model.openiam.*;
import org.openiam.apiclient.service.CurrentSessionHandler;
import org.openiam.apiclient.service.OpenIAMService;
import org.openiam.apiclient.utils.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;
import java.util.List;

@Service
public class OpenIAMServiceImpl implements OpenIAMService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public static final String LOGIN_URL = "/idp/rest/api/auth/public/login";
    public static final String REFRESH_TOKEN_URL = "/idp/oauth2/token/refresh";
    public static final String REVOKE_TOKEN_URL = "/idp/oauth2/revoke";
    public static final String CREATE_USER_URL = "/webconsole/rest/api/prov/saveUser";
    public static final String SEARCH_ROLES_URL = "/webconsole/rest/api/roles/search";
    public static final String SEARCH_GROUPS_URL = "/webconsole/rest/api/groups/search";
    public static final String SEARCH_METADATA_TYPE_URL = "/webconsole/rest/api/metadata/type/search";
    public static final String CODE = "code";
    public static final String STATE = "state";

    private final OpenIAMWebClient webClient;

    private final CurrentSessionHandler currentSessionHandler;

    private final OpenIAMDiscoveryData openIAMDiscoveryData;

    @Value("${openiamAuthenticationCookieName}")
    private String openiamAuthenticationCookieName;

    @Value("${openiamOAuthSecret}")
    private String openiamClientSecret;

    @Value("${openiamOAuthPostbackUrl}")
    private String openiamOAuthPostbackUrl;

    public OpenIAMServiceImpl(OpenIAMWebClient webClient, CurrentSessionHandler currentSessionHandler, OpenIAMDiscoveryData openIAMDiscoveryData) {
        this.webClient = webClient;
        this.currentSessionHandler = currentSessionHandler;
        this.openIAMDiscoveryData = openIAMDiscoveryData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpenIAMBaseResponse doLogin(String login, String password) throws OpenIAMLoginException {

        ResponseEntity<OpenIAMBaseResponse> loginResponse = webClient.createClient()
                .post()
                .uri(LOGIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(getLoginRequest(login, password))
                .retrieve()
                .toEntity(OpenIAMBaseResponse.class)
                .block(); // this is synchronous request, due to we can't process until we will know the result.

        if (loginResponse == null || loginResponse.getStatusCodeValue() > 299) {
            throw new OpenIAMLoginException("Communication error.");
        }

        return loginResponse.getBody();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpenIAMAuthorizationCallbackResponse doAuthorization(String cookie, String sessionId) {
        return convertToAuthorizationResponse(WebClient
                .builder()
                .baseUrl(openIAMDiscoveryData.getAuthorizationEndpoint())
                .build()
                .get()
                .uri(u ->
                        u
                                .queryParam("redirect_uri", openiamOAuthPostbackUrl)
                                .queryParam("client_id", openIAMDiscoveryData.getClientId())
                                .queryParam("response_type", CODE)
                                .queryParam(STATE, sessionId)
                                .build()
                )
                .cookie(openiamAuthenticationCookieName, cookie)
                .retrieve()
                .toBodilessEntity()
                .block());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetAccessTokenResponse doGetAccessToken(String code, String cookie) {

        ResponseEntity<GetAccessTokenResponse> getTokenResponse = WebClient.create(openIAMDiscoveryData.getTokenEndpoint())
                .post()
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromFormData(getAccessTokenRequest(code)))
                .retrieve()
                .toEntity(GetAccessTokenResponse.class)
                .block(); // this is synchronous request, due to we can't process until we will know the result.

        return getTokenResponse == null || getTokenResponse.getStatusCodeValue() != 200 ? null : getTokenResponse.getBody();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GetAccessTokenResponse doRefreshToken(String refreshToken) {
        ResponseEntity<GetAccessTokenResponse> getTokenResponse = webClient.createClient()
                .post()
                .uri(REFRESH_TOKEN_URL)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromFormData(getRefreshTokenRequest(refreshToken)))
                .retrieve()
                .toEntity(GetAccessTokenResponse.class)
                .block();
        return getTokenResponse == null || getTokenResponse.getStatusCodeValue() != 200 ? null : getTokenResponse.getBody();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean revokeToken(String token, String cookie) {
        ResponseEntity<String> revokeTokenResponse = webClient.createClient()
                .post()
                .uri(REVOKE_TOKEN_URL)
                .cookie(openiamAuthenticationCookieName, cookie)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .accept(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromFormData(getRevokeTokenRequest(token)))
                .retrieve()
                .toEntity(String.class)
                .block();
        return revokeTokenResponse != null && revokeTokenResponse.getStatusCodeValue() == HttpServletResponse.SC_OK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpenIAMBaseResponse createUser(OpenIAMCreateUserRequest openIAMCreateUserRequest) {
        ResponseEntity<OpenIAMBaseResponse> createUserResponse = webClient.createClientWithLogger()
                .post()
                .uri(CREATE_USER_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .header("Authorization", String.format("Bearer %s", currentSessionHandler.getDetails().getOpeniamOAuthToken()))
                .accept(MediaType.APPLICATION_JSON)
                .bodyValue(openIAMCreateUserRequest)
                .retrieve()
                .toEntity(OpenIAMBaseResponse.class)
                .block(); // this is synchronous request, due to we can't process until we will know the result.
        return createUserResponse == null || createUserResponse.getStatusCodeValue() != 200 ? null : createUserResponse.getBody();
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public OpenIAMBeanResponse findRoles(String name, int from, int size) {
        MultiValueMap<String, String> data = new LinkedMultiValueMap<>(3);
        data.put("name", Collections.singletonList(name));
        data.put("from", Collections.singletonList(String.valueOf(from)));
        data.put("size", Collections.singletonList(String.valueOf(size)));
        return find(data, SEARCH_ROLES_URL);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OpenIAMBeanResponse findGroups(String name, int from, int size) {
        MultiValueMap<String, String> data = new LinkedMultiValueMap<>(3);
        data.put("name", Collections.singletonList(name));
        data.put("from", Collections.singletonList(String.valueOf(from)));
        data.put("size", Collections.singletonList(String.valueOf(size)));
        return find(data, SEARCH_GROUPS_URL);
    }

    @Override
    public OpenIAMBeanResponse getMetadataTypes(String grouping, int from, int size) {
        MultiValueMap<String, String> data = new LinkedMultiValueMap<>(3);
        data.put("grouping", Collections.singletonList(grouping));
        data.put("from", Collections.singletonList(String.valueOf(from)));
        data.put("size", Collections.singletonList(String.valueOf(size)));
        return find(data, SEARCH_METADATA_TYPE_URL);
    }

    /**
     * Method is used to convert access token which should be revoked to the Appropriate HTTP request for API Call
     *
     * @param accessToken - the Access token to be revoked
     * @return - {@link MultiValueMap} with required request parameters.
     */
    private MultiValueMap<String, String> getRevokeTokenRequest(String accessToken) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("token", accessToken);
        return formData;
    }

    private MultiValueMap<String, String> getRefreshTokenRequest(String refreshToken) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("refresh_token", refreshToken);
        return formData;
    }

    private MultiValueMap<String, String> getAccessTokenRequest(String code) {
        MultiValueMap<String, String> formData = new LinkedMultiValueMap<>();
        formData.add("client_id", openIAMDiscoveryData.getClientId());
        formData.add("client_secret", openiamClientSecret);
        formData.add("redirect_uri", openiamOAuthPostbackUrl);
        formData.add("code", code);
        formData.add("grant_type", "authorization_code");
        return formData;
    }

    private OpenIAMLoginRequest getLoginRequest(String login, String password) {
        OpenIAMLoginRequest request = new OpenIAMLoginRequest();
        request.setLogin(login);
        request.setPassword(password);
        return request;
    }


    /**
     * The state and code are coming and a query parameters in the redirect URL. We should grab it from the Response
     * and put into the internal representation bean,
     *
     * @param resp - the Authorization Http Response from OpenIAM
     * @return {@link OpenIAMAuthorizationCallbackResponse} with sessionId and code from the Http Authorization Response
     */
    private OpenIAMAuthorizationCallbackResponse convertToAuthorizationResponse(@Nullable ResponseEntity<Void> resp) {
        if (resp == null) {
            return null;
        }
        final List<String> locations = resp.getHeaders().get("Location");
        final String location = locations == null ? null : locations.get(0);
        if (location == null) {
            return null;
        }
        MultiValueMap<String, String> parameters =
                UriComponentsBuilder.fromUriString(location).build().getQueryParams();
        OpenIAMAuthorizationCallbackResponse openIAMAuthorizationCallbackResponse = new OpenIAMAuthorizationCallbackResponse();
        openIAMAuthorizationCallbackResponse.setCode(CollectionUtils.isEmpty(parameters.get(CODE)) ? null : parameters.get(CODE).get(0));
        openIAMAuthorizationCallbackResponse.setSessionId(CollectionUtils.isEmpty(parameters.get(STATE)) ? null : parameters.get(STATE).get(0));
        return openIAMAuthorizationCallbackResponse;
    }

    /**
     * Internal Method that is used to find {@link OpenIAMBaseResponse} with query params from #queryParams and Url as #url
     *
     * @param queryParams - the set of query parameters for search
     * @param url         - url for search
     * @return - {@link OpenIAMBeanResponse} that contains results.
     */
    private OpenIAMBeanResponse find(MultiValueMap<String, String> queryParams, String url) {
        ResponseEntity<OpenIAMBeanResponse> findRolesResponse = webClient.createClient()
                .get()
                .uri(u ->
                        u.path(url)
                                .queryParams(queryParams)
                                .build()
                )
                .header("Authorization", String.format("Bearer %s", currentSessionHandler.getDetails().getOpeniamOAuthToken()))
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(OpenIAMBeanResponse.class)
                .block(); // this is synchronous request, due to we can't process until we will know the result.
        return findRolesResponse == null || findRolesResponse.getStatusCodeValue() != 200 ? null : findRolesResponse.getBody();
    }


}
