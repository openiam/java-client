package org.openiam.apiclient.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

/**
 * Utility service that used to create a new {@link WebClient} connected with OpenIAM
 * there are two implementation: base and with extended logging
 */
@Service
public class OpenIAMWebClient {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Value("${openiamServerAddress}")
    private String openiamServerAddress;

    /**
     * Create {@link WebClient} with baseUrl linked to OpenIAM
     *
     * @return {@link WebClient} with OpenIAM URL as a base URL
     */
    WebClient createClient() {
        return WebClient.create(openiamServerAddress);
    }

    /**
     * Create {@link WebClient} with baseUrl linked to OpenIAM and extended logging. Logs are writting as an INFO level
     *
     * @return {@link WebClient} with OpenIAM URL as a base URL
     */
    WebClient createClientWithLogger() {
        return WebClient
                .builder()
                .baseUrl(openiamServerAddress)
                .filters(exchangeFilterFunctions -> {
                    exchangeFilterFunctions.add(logRequest());
                    exchangeFilterFunctions.add(logResponse());
                })
                .build();
    }


    ExchangeFilterFunction logRequest() {
        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {
            StringBuilder sb = new StringBuilder("Request: \n");
            clientRequest
                    .headers()
                    .forEach((name, values) -> values.forEach(value -> sb.append(String.format("Header %s %s\n", name, value))));

            sb.append("Body=").append(clientRequest.body());
            logger.info(sb.toString());

            return Mono.just(clientRequest);
        });
    }

    ExchangeFilterFunction logResponse() {
        return ExchangeFilterFunction.ofResponseProcessor(clientRequest -> {
            StringBuilder sb = new StringBuilder("Request: \n");
            clientRequest
                    .headers()
                    .asHttpHeaders()
                    .forEach((name, values) -> values.forEach(value -> sb.append(String.format("Header %s %s\n", name, value))));

            sb.append("Body=").append(clientRequest.bodyToFlux(String.class));
            logger.info(sb.toString());

            return Mono.just(clientRequest);
        });
    }
}
