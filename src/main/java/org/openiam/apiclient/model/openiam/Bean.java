
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import javax.annotation.Generated;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "id",
        "beanType",
        "operation",
        "name",
        "displayName",
        "description"
})
@Generated("jsonschema2pojo")
public class Bean implements Serializable {

    @JsonProperty("id")
    private String id;
    @JsonProperty("beanType")
    private String beanType;
    @JsonProperty("operation")
    private String operation;
    @JsonProperty("name")
    private String name;
    @JsonProperty("displayName")
    private Object displayName;
    @JsonProperty("description")
    private Object description;

    private final static long serialVersionUID = 2610564871516563062L;

    @JsonProperty("id")
    public String getId() {
        return id;
    }

    @JsonProperty("id")
    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty("beanType")
    public String getBeanType() {
        return beanType;
    }

    @JsonProperty("beanType")
    public void setBeanType(String beanType) {
        this.beanType = beanType;
    }

    @JsonProperty("operation")
    public String getOperation() {
        return operation;
    }

    @JsonProperty("operation")
    public void setOperation(String operation) {
        this.operation = operation;
    }

    @JsonProperty("name")
    public String getName() {
        return name;
    }

    @JsonProperty("name")
    public void setName(String name) {
        this.name = name;
    }

    @JsonProperty("displayName")
    public Object getDisplayName() {
        return displayName;
    }

    @JsonProperty("displayName")
    public void setDisplayName(Object displayName) {
        this.displayName = displayName;
    }

    @JsonProperty("description")
    public Object getDescription() {
        return description;
    }

    @JsonProperty("description")
    public void setDescription(Object description) {
        this.description = description;
    }

}
