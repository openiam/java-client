package org.openiam.apiclient.service;

import org.openiam.apiclient.model.OpenIAMAuthorizationCallbackResponse;
import org.openiam.apiclient.model.openiam.GetAccessTokenResponse;
import org.openiam.apiclient.model.openiam.OpenIAMBaseResponse;
import org.openiam.apiclient.model.exception.OpenIAMLoginException;
import org.openiam.apiclient.model.openiam.OpenIAMBeanResponse;
import org.openiam.apiclient.model.openiam.OpenIAMCreateUserRequest;

/**
 * This is service which communicate directly with OpenIAM.
 */
public interface OpenIAMService {

    /**
     * Login against OpenIAM, send login and password. will return {@link OpenIAMBaseResponse} in case of error or success.
     *
     * @param login    - login
     * @param password - password
     * @return {@link OpenIAMBaseResponse} with errors or with OpenIAM cookie token in {@link OpenIAMBaseResponse#getTokenInfo()}
     * @throws OpenIAMLoginException
     */
    OpenIAMBaseResponse doLogin(String login, String password) throws OpenIAMLoginException;

    /**
     * Authorize user against OpenIAM Oauth call.
     *
     * @param cookie    - the OpenIAM authentication cookie
     * @param sessionId - the current client app sessionId, identified with the cookie.
     * @return {@link OpenIAMAuthorizationCallbackResponse} with #sessionId and authorization code.
     */
    OpenIAMAuthorizationCallbackResponse doAuthorization(String cookie, String sessionId);


    /**
     * Get Access token from the OpenIAM.
     *
     * @param code   - the authirzation token retrieved with {@link OpenIAMService#doAuthorization(String, String)}
     * @param cookie - the OpenIAM authentication cookie
     * @return
     */
    GetAccessTokenResponse doGetAccessToken(String code, String cookie);

    /**
     * Get New Access token based on refresh Token.
     *
     * @param refreshToken - the refresh token provided with Response by {@link OpenIAMService#doAuthorization(String, String)}
     * @return {@link GetAccessTokenResponse} with new Access token and Refresh Token
     */
    GetAccessTokenResponse doRefreshToken(String refreshToken);

    /**
     * Revoke Oauth2 access token against OpenIAM
     *
     * @param token  - the access token provided to be revoked.
     * @param cookie - OpenIAM Authentication Cookie
     * @return boolean in case revokation was success
     */
    boolean revokeToken(String token, String cookie);

    /**
     * Method is used to create a new User In OpenIAM
     *
     * @param openIAMCreateUserRequest - the {@link OpenIAMCreateUserRequest} with data for user to be saved.
     * @return {@link OpenIAMBaseResponse}. To detect the success please check {@link OpenIAMBaseResponse#getStatus()}.
     * Status 200 is a feature of success.
     * {@link OpenIAMBaseResponse#getErrorList()} contains an errors in case of {@link OpenIAMBaseResponse#getStatus()} is not equals 200
     */
    OpenIAMBaseResponse createUser(OpenIAMCreateUserRequest openIAMCreateUserRequest);

    /**
     * Use to search for Roles
     *
     * @param name - the term to search roles by name
     * @param from - very 1st request
     * @param size - max number of result per request
     * @return {@link OpenIAMBeanResponse} or null in case of any errors
     */
    OpenIAMBeanResponse findRoles(String name, int from, int size);

    /**
     * Use to search for Groups
     *
     * @param name - the term to search groups by name
     * @param from - very 1st request
     * @param size - max number of result per request
     * @return {@link OpenIAMBeanResponse} or null in case of any errors
     */
    OpenIAMBeanResponse findGroups(String name, int from, int size);

    /**
     * Use to search for Groups
     *
     * @param grouping - the grouping of MetadataType we should get
     * @param from - very 1st request
     * @param size - max number of result per request
     * @return {@link OpenIAMBeanResponse} or null in case of any errors
     */
    OpenIAMBeanResponse getMetadataTypes(String grouping, int from, int size);

}
