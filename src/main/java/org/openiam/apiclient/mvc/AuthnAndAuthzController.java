package org.openiam.apiclient.mvc;

import org.openiam.apiclient.filter.OpenIAMAuthorizationTokenFilter;
import org.openiam.apiclient.model.BaseClientAppResponse;
import org.openiam.apiclient.model.ClientAppSessionDetails;
import org.openiam.apiclient.service.SessionManagementService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;

@org.springframework.stereotype.Controller
public class AuthnAndAuthzController {

    private final SessionManagementService sessionManagementService;


    public AuthnAndAuthzController(SessionManagementService sessionManagementService) {
        this.sessionManagementService = sessionManagementService;
    }

    @GetMapping(value = {"/"})
    public String main(HttpServletRequest request) {
        ClientAppSessionDetails details = sessionManagementService.getCurrentSessionDetails(request);
        request.setAttribute("OPENIAM_COOKIE", details.getOpeniamCookieToken());
        request.setAttribute("OPENIAM_ACCESS_TOKEN", details.getOpeniamOAuthToken());
        request.setAttribute("OPENIAM_REFRESH_TOKEN", details.getOpeniamOAuthRefreshToken());
        request.setAttribute("OPENIAM_USER_ID", details.getUserId());
        return "index";
    }

    @GetMapping(value = {"/login"})
    public String login(HttpServletRequest request) {
        request.setAttribute(OpenIAMAuthorizationTokenFilter.REDIRECT_PARAM, request.getParameter(OpenIAMAuthorizationTokenFilter.REDIRECT_PARAM));
        return "login";
    }

    @GetMapping(value = {"/logout"})
    public String logout(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sessionManagementService.logout(request);
        response.sendRedirect("/");
        return null;
    }

    @GetMapping(value = {"/error"})
    public String error(HttpServletRequest request, HttpServletResponse response) throws IOException {
        request.setAttribute("errors", Collections.singletonList("Access denied to this Resource"));
        return "error";
    }


    @PostMapping(value = {"/login"})
    public String postLogin(HttpServletRequest request, HttpServletResponse servletResponse) throws IOException {
        BaseClientAppResponse dataResponse = sessionManagementService.authenticate(request);
        if (!dataResponse.isSuccess()) {
            request.setAttribute("errors", dataResponse.getErrors());
            request.setAttribute(OpenIAMAuthorizationTokenFilter.REDIRECT_PARAM, request.getParameter(OpenIAMAuthorizationTokenFilter.REDIRECT_PARAM));
            return "error";
        } else {
            sessionManagementService.saveSessionCookie(servletResponse, dataResponse.getSessionId());
            if (request.getParameter(OpenIAMAuthorizationTokenFilter.REDIRECT_PARAM) == null) {
                servletResponse.sendRedirect("/");
            } else {
                servletResponse.sendRedirect(request.getParameter(OpenIAMAuthorizationTokenFilter.REDIRECT_PARAM));
            }
            return null;
        }
    }

    @PostMapping(value = "/refresh-oauth-token")
    public String refreshToken(HttpServletRequest request, HttpServletResponse response) throws IOException {
        sessionManagementService.refreshOAuthAccessToken(request);
        response.sendRedirect("/");
        return null;
    }

}
