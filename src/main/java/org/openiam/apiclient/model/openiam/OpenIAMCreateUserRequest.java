
package org.openiam.apiclient.model.openiam;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Generated;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "emailAddresses",
    "addresses",
    "phones",
    "attributes",
    "firstName",
    "middleInit",
    "lastName",
    "nickname",
    "maidenName",
    "suffix",
    "sex",
    "title",
    "jobCodeId",
    "classification",
    "employeeId",
    "userTypeInd",
    "employeeTypeId",
    "visible",
    "birthdateAsStr",
    "startDateAsStr",
    "lastDateAsStr",
    "alternativeStartDateAsStr",
    "alternativeEndDateAsStr",
    "alternateContactId",
    "certificationDelegateId",
    "certificationDelegateStartDateAsStr",
    "certificationDelegateEndDateAsStr",
    "sameUserId",
    "login",
    "password",
    "supervisorId",
    "supervisorMetadataTypeId",
    "organizationIds",
    "roleIds",
    "groupIds",
    "metadataTypeId",
    "notifyUserViaEmail",
    "notifySupervisorViaEmail",
    "provisionOnStartDate"
})
@Generated("jsonschema2pojo")
public class OpenIAMCreateUserRequest implements Serializable
{

    @JsonProperty("emailAddresses")
    private List<EmailAddress> emailAddresses = new ArrayList<EmailAddress>();
    @JsonProperty("addresses")
    private List<Object> addresses = new ArrayList<Object>();
    @JsonProperty("phones")
    private List<Phone> phones = new ArrayList<Phone>();
    @JsonProperty("attributes")
    private List<Attribute> attributes = new ArrayList<Attribute>();
    @JsonProperty("firstName")
    private String firstName;
    @JsonProperty("middleInit")
    private String middleInit;
    @JsonProperty("lastName")
    private String lastName;
    @JsonProperty("nickname")
    private String nickname;
    @JsonProperty("maidenName")
    private String maidenName;
    @JsonProperty("suffix")
    private String suffix;
    @JsonProperty("sex")
    private String sex;
    @JsonProperty("title")
    private String title;
    @JsonProperty("jobCodeId")
    private String jobCodeId;
    @JsonProperty("classification")
    private String classification;
    @JsonProperty("employeeId")
    private String employeeId;
    @JsonProperty("userTypeInd")
    private String userTypeInd;
    @JsonProperty("employeeTypeId")
    private String employeeTypeId;
    @JsonProperty("visible")
    private Boolean visible;
    @JsonProperty("birthdateAsStr")
    private Object birthdateAsStr;
    @JsonProperty("startDateAsStr")
    private Object startDateAsStr;
    @JsonProperty("lastDateAsStr")
    private Object lastDateAsStr;
    @JsonProperty("alternativeStartDateAsStr")
    private Object alternativeStartDateAsStr;
    @JsonProperty("alternativeEndDateAsStr")
    private Object alternativeEndDateAsStr;
    @JsonProperty("alternateContactId")
    private Object alternateContactId;
    @JsonProperty("certificationDelegateId")
    private Object certificationDelegateId;
    @JsonProperty("certificationDelegateStartDateAsStr")
    private Object certificationDelegateStartDateAsStr;
    @JsonProperty("certificationDelegateEndDateAsStr")
    private Object certificationDelegateEndDateAsStr;
    @JsonProperty("sameUserId")
    private Object sameUserId;
    @JsonProperty("login")
    private String login;
    @JsonProperty("password")
    private String password;
    @JsonProperty("supervisorId")
    private Object supervisorId;
    @JsonProperty("supervisorMetadataTypeId")
    private String supervisorMetadataTypeId;
    @JsonProperty("organizationIds")
    private List<Object> organizationIds = new ArrayList<Object>();
    @JsonProperty("roleIds")
    private List<String> roleIds = new ArrayList<String>();
    @JsonProperty("groupIds")
    private List<String> groupIds = new ArrayList<String>();
    @JsonProperty("metadataTypeId")
    private String metadataTypeId;
    @JsonProperty("notifyUserViaEmail")
    private Boolean notifyUserViaEmail;
    @JsonProperty("notifySupervisorViaEmail")
    private Boolean notifySupervisorViaEmail;
    @JsonProperty("provisionOnStartDate")
    private Boolean provisionOnStartDate;
    private final static long serialVersionUID = 4143000451811398291L;

    @JsonProperty("emailAddresses")
    public List<EmailAddress> getEmailAddresses() {
        return emailAddresses;
    }

    @JsonProperty("emailAddresses")
    public void setEmailAddresses(List<EmailAddress> emailAddresses) {
        this.emailAddresses = emailAddresses;
    }

    @JsonProperty("addresses")
    public List<Object> getAddresses() {
        return addresses;
    }

    @JsonProperty("addresses")
    public void setAddresses(List<Object> addresses) {
        this.addresses = addresses;
    }

    @JsonProperty("phones")
    public List<Phone> getPhones() {
        return phones;
    }

    @JsonProperty("phones")
    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @JsonProperty("attributes")
    public List<Attribute> getAttributes() {
        return attributes;
    }

    @JsonProperty("attributes")
    public void setAttributes(List<Attribute> attributes) {
        this.attributes = attributes;
    }

    @JsonProperty("firstName")
    public String getFirstName() {
        return firstName;
    }

    @JsonProperty("firstName")
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @JsonProperty("middleInit")
    public String getMiddleInit() {
        return middleInit;
    }

    @JsonProperty("middleInit")
    public void setMiddleInit(String middleInit) {
        this.middleInit = middleInit;
    }

    @JsonProperty("lastName")
    public String getLastName() {
        return lastName;
    }

    @JsonProperty("lastName")
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @JsonProperty("nickname")
    public String getNickname() {
        return nickname;
    }

    @JsonProperty("nickname")
    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    @JsonProperty("maidenName")
    public String getMaidenName() {
        return maidenName;
    }

    @JsonProperty("maidenName")
    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    @JsonProperty("suffix")
    public String getSuffix() {
        return suffix;
    }

    @JsonProperty("suffix")
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    @JsonProperty("sex")
    public String getSex() {
        return sex;
    }

    @JsonProperty("sex")
    public void setSex(String sex) {
        this.sex = sex;
    }

    @JsonProperty("title")
    public String getTitle() {
        return title;
    }

    @JsonProperty("title")
    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty("jobCodeId")
    public String getJobCodeId() {
        return jobCodeId;
    }

    @JsonProperty("jobCodeId")
    public void setJobCodeId(String jobCodeId) {
        this.jobCodeId = jobCodeId;
    }

    @JsonProperty("classification")
    public String getClassification() {
        return classification;
    }

    @JsonProperty("classification")
    public void setClassification(String classification) {
        this.classification = classification;
    }

    @JsonProperty("employeeId")
    public String getEmployeeId() {
        return employeeId;
    }

    @JsonProperty("employeeId")
    public void setEmployeeId(String employeeId) {
        this.employeeId = employeeId;
    }

    @JsonProperty("userTypeInd")
    public String getUserTypeInd() {
        return userTypeInd;
    }

    @JsonProperty("userTypeInd")
    public void setUserTypeInd(String userTypeInd) {
        this.userTypeInd = userTypeInd;
    }

    @JsonProperty("employeeTypeId")
    public String getEmployeeTypeId() {
        return employeeTypeId;
    }

    @JsonProperty("employeeTypeId")
    public void setEmployeeTypeId(String employeeTypeId) {
        this.employeeTypeId = employeeTypeId;
    }

    @JsonProperty("visible")
    public Boolean getVisible() {
        return visible;
    }

    @JsonProperty("visible")
    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    @JsonProperty("birthdateAsStr")
    public Object getBirthdateAsStr() {
        return birthdateAsStr;
    }

    @JsonProperty("birthdateAsStr")
    public void setBirthdateAsStr(Object birthdateAsStr) {
        this.birthdateAsStr = birthdateAsStr;
    }

    @JsonProperty("startDateAsStr")
    public Object getStartDateAsStr() {
        return startDateAsStr;
    }

    @JsonProperty("startDateAsStr")
    public void setStartDateAsStr(Object startDateAsStr) {
        this.startDateAsStr = startDateAsStr;
    }

    @JsonProperty("lastDateAsStr")
    public Object getLastDateAsStr() {
        return lastDateAsStr;
    }

    @JsonProperty("lastDateAsStr")
    public void setLastDateAsStr(Object lastDateAsStr) {
        this.lastDateAsStr = lastDateAsStr;
    }

    @JsonProperty("alternativeStartDateAsStr")
    public Object getAlternativeStartDateAsStr() {
        return alternativeStartDateAsStr;
    }

    @JsonProperty("alternativeStartDateAsStr")
    public void setAlternativeStartDateAsStr(Object alternativeStartDateAsStr) {
        this.alternativeStartDateAsStr = alternativeStartDateAsStr;
    }

    @JsonProperty("alternativeEndDateAsStr")
    public Object getAlternativeEndDateAsStr() {
        return alternativeEndDateAsStr;
    }

    @JsonProperty("alternativeEndDateAsStr")
    public void setAlternativeEndDateAsStr(Object alternativeEndDateAsStr) {
        this.alternativeEndDateAsStr = alternativeEndDateAsStr;
    }

    @JsonProperty("alternateContactId")
    public Object getAlternateContactId() {
        return alternateContactId;
    }

    @JsonProperty("alternateContactId")
    public void setAlternateContactId(Object alternateContactId) {
        this.alternateContactId = alternateContactId;
    }

    @JsonProperty("certificationDelegateId")
    public Object getCertificationDelegateId() {
        return certificationDelegateId;
    }

    @JsonProperty("certificationDelegateId")
    public void setCertificationDelegateId(Object certificationDelegateId) {
        this.certificationDelegateId = certificationDelegateId;
    }

    @JsonProperty("certificationDelegateStartDateAsStr")
    public Object getCertificationDelegateStartDateAsStr() {
        return certificationDelegateStartDateAsStr;
    }

    @JsonProperty("certificationDelegateStartDateAsStr")
    public void setCertificationDelegateStartDateAsStr(Object certificationDelegateStartDateAsStr) {
        this.certificationDelegateStartDateAsStr = certificationDelegateStartDateAsStr;
    }

    @JsonProperty("certificationDelegateEndDateAsStr")
    public Object getCertificationDelegateEndDateAsStr() {
        return certificationDelegateEndDateAsStr;
    }

    @JsonProperty("certificationDelegateEndDateAsStr")
    public void setCertificationDelegateEndDateAsStr(Object certificationDelegateEndDateAsStr) {
        this.certificationDelegateEndDateAsStr = certificationDelegateEndDateAsStr;
    }

    @JsonProperty("sameUserId")
    public Object getSameUserId() {
        return sameUserId;
    }

    @JsonProperty("sameUserId")
    public void setSameUserId(Object sameUserId) {
        this.sameUserId = sameUserId;
    }

    @JsonProperty("login")
    public String getLogin() {
        return login;
    }

    @JsonProperty("login")
    public void setLogin(String login) {
        this.login = login;
    }

    @JsonProperty("password")
    public String getPassword() {
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }

    @JsonProperty("supervisorId")
    public Object getSupervisorId() {
        return supervisorId;
    }

    @JsonProperty("supervisorId")
    public void setSupervisorId(Object supervisorId) {
        this.supervisorId = supervisorId;
    }

    @JsonProperty("supervisorMetadataTypeId")
    public String getSupervisorMetadataTypeId() {
        return supervisorMetadataTypeId;
    }

    @JsonProperty("supervisorMetadataTypeId")
    public void setSupervisorMetadataTypeId(String supervisorMetadataTypeId) {
        this.supervisorMetadataTypeId = supervisorMetadataTypeId;
    }

    @JsonProperty("organizationIds")
    public List<Object> getOrganizationIds() {
        return organizationIds;
    }

    @JsonProperty("organizationIds")
    public void setOrganizationIds(List<Object> organizationIds) {
        this.organizationIds = organizationIds;
    }

    @JsonProperty("roleIds")
    public List<String> getRoleIds() {
        return roleIds;
    }

    @JsonProperty("roleIds")
    public void setRoleIds(List<String> roleIds) {
        this.roleIds = roleIds;
    }

    @JsonProperty("groupIds")
    public List<String> getGroupIds() {
        return groupIds;
    }

    @JsonProperty("groupIds")
    public void setGroupIds(List<String> groupIds) {
        this.groupIds = groupIds;
    }

    @JsonProperty("metadataTypeId")
    public String getMetadataTypeId() {
        return metadataTypeId;
    }

    @JsonProperty("metadataTypeId")
    public void setMetadataTypeId(String metadataTypeId) {
        this.metadataTypeId = metadataTypeId;
    }

    @JsonProperty("notifyUserViaEmail")
    public Boolean getNotifyUserViaEmail() {
        return notifyUserViaEmail;
    }

    @JsonProperty("notifyUserViaEmail")
    public void setNotifyUserViaEmail(Boolean notifyUserViaEmail) {
        this.notifyUserViaEmail = notifyUserViaEmail;
    }

    @JsonProperty("notifySupervisorViaEmail")
    public Boolean getNotifySupervisorViaEmail() {
        return notifySupervisorViaEmail;
    }

    @JsonProperty("notifySupervisorViaEmail")
    public void setNotifySupervisorViaEmail(Boolean notifySupervisorViaEmail) {
        this.notifySupervisorViaEmail = notifySupervisorViaEmail;
    }

    @JsonProperty("provisionOnStartDate")
    public Boolean getProvisionOnStartDate() {
        return provisionOnStartDate;
    }

    @JsonProperty("provisionOnStartDate")
    public void setProvisionOnStartDate(Boolean provisionOnStartDate) {
        this.provisionOnStartDate = provisionOnStartDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(OpenIAMCreateUserRequest.class.getName()).append('@').append(Integer.toHexString(System.identityHashCode(this))).append('[');
        sb.append("emailAddresses");
        sb.append('=');
        sb.append(((this.emailAddresses == null)?"<null>":this.emailAddresses));
        sb.append(',');
        sb.append("addresses");
        sb.append('=');
        sb.append(((this.addresses == null)?"<null>":this.addresses));
        sb.append(',');
        sb.append("phones");
        sb.append('=');
        sb.append(((this.phones == null)?"<null>":this.phones));
        sb.append(',');
        sb.append("attributes");
        sb.append('=');
        sb.append(((this.attributes == null)?"<null>":this.attributes));
        sb.append(',');
        sb.append("firstName");
        sb.append('=');
        sb.append(((this.firstName == null)?"<null>":this.firstName));
        sb.append(',');
        sb.append("middleInit");
        sb.append('=');
        sb.append(((this.middleInit == null)?"<null>":this.middleInit));
        sb.append(',');
        sb.append("lastName");
        sb.append('=');
        sb.append(((this.lastName == null)?"<null>":this.lastName));
        sb.append(',');
        sb.append("nickname");
        sb.append('=');
        sb.append(((this.nickname == null)?"<null>":this.nickname));
        sb.append(',');
        sb.append("maidenName");
        sb.append('=');
        sb.append(((this.maidenName == null)?"<null>":this.maidenName));
        sb.append(',');
        sb.append("suffix");
        sb.append('=');
        sb.append(((this.suffix == null)?"<null>":this.suffix));
        sb.append(',');
        sb.append("sex");
        sb.append('=');
        sb.append(((this.sex == null)?"<null>":this.sex));
        sb.append(',');
        sb.append("title");
        sb.append('=');
        sb.append(((this.title == null)?"<null>":this.title));
        sb.append(',');
        sb.append("jobCodeId");
        sb.append('=');
        sb.append(((this.jobCodeId == null)?"<null>":this.jobCodeId));
        sb.append(',');
        sb.append("classification");
        sb.append('=');
        sb.append(((this.classification == null)?"<null>":this.classification));
        sb.append(',');
        sb.append("employeeId");
        sb.append('=');
        sb.append(((this.employeeId == null)?"<null>":this.employeeId));
        sb.append(',');
        sb.append("userTypeInd");
        sb.append('=');
        sb.append(((this.userTypeInd == null)?"<null>":this.userTypeInd));
        sb.append(',');
        sb.append("employeeTypeId");
        sb.append('=');
        sb.append(((this.employeeTypeId == null)?"<null>":this.employeeTypeId));
        sb.append(',');
        sb.append("visible");
        sb.append('=');
        sb.append(((this.visible == null)?"<null>":this.visible));
        sb.append(',');
        sb.append("birthdateAsStr");
        sb.append('=');
        sb.append(((this.birthdateAsStr == null)?"<null>":this.birthdateAsStr));
        sb.append(',');
        sb.append("startDateAsStr");
        sb.append('=');
        sb.append(((this.startDateAsStr == null)?"<null>":this.startDateAsStr));
        sb.append(',');
        sb.append("lastDateAsStr");
        sb.append('=');
        sb.append(((this.lastDateAsStr == null)?"<null>":this.lastDateAsStr));
        sb.append(',');
        sb.append("alternativeStartDateAsStr");
        sb.append('=');
        sb.append(((this.alternativeStartDateAsStr == null)?"<null>":this.alternativeStartDateAsStr));
        sb.append(',');
        sb.append("alternativeEndDateAsStr");
        sb.append('=');
        sb.append(((this.alternativeEndDateAsStr == null)?"<null>":this.alternativeEndDateAsStr));
        sb.append(',');
        sb.append("alternateContactId");
        sb.append('=');
        sb.append(((this.alternateContactId == null)?"<null>":this.alternateContactId));
        sb.append(',');
        sb.append("certificationDelegateId");
        sb.append('=');
        sb.append(((this.certificationDelegateId == null)?"<null>":this.certificationDelegateId));
        sb.append(',');
        sb.append("certificationDelegateStartDateAsStr");
        sb.append('=');
        sb.append(((this.certificationDelegateStartDateAsStr == null)?"<null>":this.certificationDelegateStartDateAsStr));
        sb.append(',');
        sb.append("certificationDelegateEndDateAsStr");
        sb.append('=');
        sb.append(((this.certificationDelegateEndDateAsStr == null)?"<null>":this.certificationDelegateEndDateAsStr));
        sb.append(',');
        sb.append("sameUserId");
        sb.append('=');
        sb.append(((this.sameUserId == null)?"<null>":this.sameUserId));
        sb.append(',');
        sb.append("login");
        sb.append('=');
        sb.append(((this.login == null)?"<null>":this.login));
        sb.append(',');
        sb.append("password");
        sb.append('=');
        sb.append(((this.password == null)?"<null>":this.password));
        sb.append(',');
        sb.append("supervisorId");
        sb.append('=');
        sb.append(((this.supervisorId == null)?"<null>":this.supervisorId));
        sb.append(',');
        sb.append("supervisorMetadataTypeId");
        sb.append('=');
        sb.append(((this.supervisorMetadataTypeId == null)?"<null>":this.supervisorMetadataTypeId));
        sb.append(',');
        sb.append("organizationIds");
        sb.append('=');
        sb.append(((this.organizationIds == null)?"<null>":this.organizationIds));
        sb.append(',');
        sb.append("roleIds");
        sb.append('=');
        sb.append(((this.roleIds == null)?"<null>":this.roleIds));
        sb.append(',');
        sb.append("groupIds");
        sb.append('=');
        sb.append(((this.groupIds == null)?"<null>":this.groupIds));
        sb.append(',');
        sb.append("metadataTypeId");
        sb.append('=');
        sb.append(((this.metadataTypeId == null)?"<null>":this.metadataTypeId));
        sb.append(',');
        sb.append("notifyUserViaEmail");
        sb.append('=');
        sb.append(((this.notifyUserViaEmail == null)?"<null>":this.notifyUserViaEmail));
        sb.append(',');
        sb.append("notifySupervisorViaEmail");
        sb.append('=');
        sb.append(((this.notifySupervisorViaEmail == null)?"<null>":this.notifySupervisorViaEmail));
        sb.append(',');
        sb.append("provisionOnStartDate");
        sb.append('=');
        sb.append(((this.provisionOnStartDate == null)?"<null>":this.provisionOnStartDate));
        sb.append(',');
        if (sb.charAt((sb.length()- 1)) == ',') {
            sb.setCharAt((sb.length()- 1), ']');
        } else {
            sb.append(']');
        }
        return sb.toString();
    }

}
